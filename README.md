# GIS WebApp

## Setting up Green Actions Map Webapp in development environment on local machine

**Note: Do not use this setup in production-like environments.**

### Install PostgreSQL server

https://www.postgresql.org/download/linux/ubuntu/

```shell
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql-13 postgresql-server-dev-13 postgresql-client-13
```

# Configure PostgreSQL server

```
local   green_actions_map_webapp_database     green_actions_map_webapp_database_user                 trust
local   all             postgres                                trust
local   all             all                                     trust
host    green_actions_map_webapp_database     green_actions_map_webapp_database_user 127.0.0.1/32    trust
host    all             all             127.0.0.1/32            trust
host    all             all             ::1/128                 trust
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```

```
listen_addresses = '*'          # what IP address(es) to listen on;
                                        # comma-separated list of addresses;
                                        # defaults to 'localhost'; use '*' for all
                                        # (change requires restart)
port = 5432                             # (change requires restart
```

### Start PostgreSQL server

```shell
systemctl start postgresql@13-main.service
```

#### Add UbuntuGIS repository

```shell
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
```

#### Install pgrouting

```shell
sudo apt-get -y install postgresql-13-pgrouting
```

#### Install geos

```shell
sudo apt-get -y install libgeos-3.8.1
```

#### Install proj

```shell
sudo apt-get -y install proj-bin proj-data
```

#### Install gdal

```shell
sudo apt-get -y install gdal-bin gdal-data
```

#### Install postgis

```shell
sudo apt-get -y install postgresql-13-postgis-3 postgresql-13-postgis-3-scripts
```

#### Enable postgresql extensions

```shell
createdb -h 127.0.0.1 -p 5432 -U green_actions_map_webapp_database_user routing
psql -h 127.0.0.1 -p 5432 -U green_actions_map_webapp_database_user routing -c 'CREATE EXTENSION PostGIS'
psql -h 127.0.0.1 -p 5432 -U green_actions_map_webapp_database_user routing -c 'CREATE EXTENSION pgRouting'
```

### Install pgAdmin4

https://www.pgadmin.org/download/pgadmin-4-apt/

```shell
curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
sudo apt install pgadmin4
sudo /usr/pgadmin4/bin/pgadmin4
```

```
http://127.0.0.1:42833
```

### Create User

```
CREATE ROLE green_actions_map_webapp_database_user WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'changeme';
```

### Create Database

```
CREATE DATABASE green_actions_map_webapp_database
    WITH 
    OWNER = green_actions_map_webapp_database_user
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
```

### Change Django settings file

```python
ALLOWED_HOSTS = [ "127.0.0.1" ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'green_actions_map_webapp_database',
        'USER': 'green_actions_map_webapp_database_user',
        'PASSWORD': 'changeme',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

### Install psycopg2

https://pypi.org/project/psycopg2/

```shell
PIPENV_VENV_IN_PROJECT=1 pipenv run pip install Django uwsgi psycopg2
```

### Execute migrations

```shell
PIPENV_VENV_IN_PROJECT=1 pipenv run python3 manage.py migrate
```

### Create user with password

```shell
PIPENV_VENV_IN_PROJECT=1 pipenv run python3 manage.py createsuperuser --username admin
```

### Execute webapp

```shell
PIPENV_VENV_IN_PROJECT=1 pipenv run python3 manage.py runserver
```

### Open webapp in browser

```
http://127.0.0.1:8000/admin
```
